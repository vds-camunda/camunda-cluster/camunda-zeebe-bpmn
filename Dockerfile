FROM maven:3.8.3-openjdk-17 AS maven-builder
COPY ./ ./
RUN mvn clean package -DskipTests

FROM openjdk:17.0.2-jdk-slim AS simple-spring-boot-app
COPY --from=maven-builder /target/*.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]