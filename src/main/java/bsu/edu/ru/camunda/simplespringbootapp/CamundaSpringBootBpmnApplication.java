package bsu.edu.ru.camunda.simplespringbootapp;

import io.camunda.zeebe.spring.client.annotation.Deployment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Deployment
@SpringBootApplication
public class CamundaSpringBootBpmnApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamundaSpringBootBpmnApplication.class, args);
    }

}
